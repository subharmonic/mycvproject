﻿
// MycvProjectView.cpp : CMycvProjectView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MycvProject.h"
#endif

#include "MycvProjectDoc.h"
#include "MycvProjectView.h"
#include "AreaDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define DEFAULT_IGNORE 50
#define DEFAULT_THRESHOLD 125

// CMycvProjectView

//IMPLEMENT_DYNCREATE(CMycvProjectView, CView)
IMPLEMENT_DYNCREATE(CMycvProjectView, CScrollView)

BEGIN_MESSAGE_MAP(CMycvProjectView, CScrollView)
	ON_COMMAND(ID_Refresh, &CMycvProjectView::OnRefresh)
	ON_COMMAND(ID_AREACALC, &CMycvProjectView::OnAreacalc)
	ON_COMMAND(ID_PARADJUST, &CMycvProjectView::OnParadjust)
	ON_COMMAND(ID_FILE_SAVE_AS,&CMycvProjectView::OnFileSaveAs)
	ON_UPDATE_COMMAND_UI(ID_Refresh, &CMycvProjectView::OnUpdateRefresh)
	ON_UPDATE_COMMAND_UI(ID_AREACALC,&CMycvProjectView::OnUpdateAreacalc)
	ON_UPDATE_COMMAND_UI(ID_PARADJUST,&CMycvProjectView::OnUpdateParadjust)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS,&CMycvProjectView::OnUpdateFileSaveAs)
END_MESSAGE_MAP()

// CMycvProjectView 构造/析构

CMycvProjectView::CMycvProjectView()
{
	// TODO: 在此处添加构造代码
	saveImg    = NULL;
	workImg    = NULL;
  
	m_lpBmi    = 0;

	m_dibFlag  = 0;
 	m_ImageType= 0;
  
	isFirstRun = TRUE;
	isSettingChanged = FALSE;
	CSize sizeTotal;
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);

	padjust.iIgnore = DEFAULT_IGNORE;
	padjust.iThreshold = DEFAULT_THRESHOLD;
	padjust.CheckBoxState = 0;
}

CMycvProjectView::~CMycvProjectView()
{
	/*if (m_CaptFlag)
		AbortCapture(workImg);            */  //  关闭视频

 	if (saveImg)
		cvReleaseImage(&saveImg);           //  释放位图
 	if (workImg)
		cvReleaseImage(&workImg);
 
	if (m_lpBmi)
		free(m_lpBmi);                      //  释放位图信息

	//if (captSetFlag==1) {
	//	if(fCapture.Open( "CaptSetup.txt", CFile::modeCreate | 
	//		CFile::modeWrite, &eCapture ) )
	//	{
	//		sprintf(pbuf,"%d  %d",frameSetW,frameSetH);
	//		fCapture.Write( pbuf, 20 );     //  分辨率设置数据存盘
	//		fCapture.Close();
	//	}
	//	captSetFlag=0;
	//}
}

BOOL CMycvProjectView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CScrollView::PreCreateWindow(cs);
}

// CMycvProjectView 绘制

void CMycvProjectView::OnDraw(CDC* pDC)
{
	CMycvProjectDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	// TODO: add draw code for native data here
	if (pDoc->pImg!=NULL)	{	            //  有磁盘输入图像
		if (pDoc->m_Display==0) {           //  尚未显示
 			imageClone(pDoc->pImg,&saveImg);         //  复制到备份位图
			m_dibFlag=imageClone(saveImg,&workImg);  //  复制到工作位图

			m_ImageType=imageType(workImg);
			m_SaveFlag=m_ImageType;
			pDoc->m_Display=1;
		}
	}
 
	if (m_dibFlag) {                        //  DIB 结构改变
 		if (m_lpBmi)
			free(m_lpBmi);
		m_lpBmi=CtreateMapInfo(workImg,m_dibFlag);
		m_dibFlag=0;

		CSize  sizeTotal;
 		sizeTotal = CSize(workImg->width,workImg->height);
		SetScrollSizes(MM_TEXT,sizeTotal);  //  设置滚动条
	}

	char *pBits = NULL;
	if (workImg)  pBits=workImg->imageData;

	if (workImg) {                          //  刷新窗口画面
		StretchDIBits(pDC->m_hDC,
			    0,0,workImg->width,workImg->height,
				0,0,workImg->width,workImg->height,
				pBits,m_lpBmi,DIB_RGB_COLORS,SRCCOPY);
	}

}
void CMycvProjectView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
}

// CMycvProjectView 诊断

#ifdef _DEBUG
void CMycvProjectView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CMycvProjectView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CMycvProjectDoc* CMycvProjectView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMycvProjectDoc)));
	return (CMycvProjectDoc*)m_pDocument;
}
#endif //_DEBUG


// CMycvProjectView 消息处理程序
void CMycvProjectView::OnSize(UINT nType, int cx, int cy) 
{
	CScrollView::OnSize(nType, cx, cy);
	
	if (workImg) {                          //  刷新窗口画面
 		CSize  sizeTotal;
 		sizeTotal = CSize(workImg->width,workImg->height);
		SetScrollSizes(MM_TEXT, sizeTotal);   //  设置滚动条
	}
}

//函数功能：恢复原图像
void CMycvProjectView::OnRefresh()
{
	// TODO: 在此添加命令处理程序代码
	m_dibFlag=imageClone(saveImg,&workImg);
	m_ImageType=m_SaveFlag;
  	Invalidate();
}

//函数功能：基本全局阈值化
int CMycvProjectView::OnBasicglobalthreshold()
{
	// TODO: 在此添加命令处理程序代码
	int  pg[256],i,thre;

	for (i=0;i<256;i++) pg[i]=0;
	for (i=0;i<workImg->imageSize;i++)      //  直方图统计
		pg[(BYTE)workImg->imageData[i]]++;

	thre = BasicGlobalThreshold(pg,0,256);    //  确定阈值
	return thre;
}
//
////函数功能：把彩色图像变成灰阶图像
//void CMycvProjectView::OnColortogray()
//{
//	// TODO: 在此添加命令处理程序代码
//	IplImage* pImage;
//	IplImage* pImg8u = NULL;
// 
//	pImage = workImg;                       //  待处理图像换名
//
//    //  建立辅助位图
// 	pImg8u = cvCreateImage(cvGetSize(pImage),IPL_DEPTH_8U,1);
//
// 	cvCvtColor(pImage,pImg8u,CV_BGR2GRAY);  //  彩色变灰阶
//
//	m_dibFlag=imageReplace(pImg8u,&workImg);  //  输出处理结果
// 
// 	imageClone(workImg,&saveImg);           //  保存当前位图
//     
//	m_SaveFlag=m_ImageType=1;               //  设置灰阶位图标志
//	Invalidate();
//}
//
////函数功能：二维OTSU法对图像进行灰度阈值变换
//void CMycvProjectView::OnTdotsu()
//{
//	// TODO: 在此添加命令处理程序代码
//	int thre;
//	IplImage* pImage = NULL;
//	pImage = workImg;
//	thre=TwoDimentionOtsu(pImage,workImg->width,workImg->height);    //  确定阈值
//
//	cvThreshold(workImg,workImg,thre,255,CV_THRESH_BINARY);  //  二值化
//
//	m_ImageType=-1;
// 	Invalidate();
//}

//函数功能：计算连通区面积
void CMycvProjectView::OnAreacalc()
{
	// TODO: 在此添加命令处理程序代码
	int  mode = CV_RETR_CCOMP;
	int  contours_num = 0,color;
	CvScalar external_color;
	CvScalar hole_color;
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvSeq* contour = 0;
	IplImage* pImg;
	IplImage* pContourImg = NULL;
	CvFont  font;
	
	if(!isFirstRun && !isSettingChanged)
	{
		if(IDOK==AfxMessageBox("参数设置未改变，使用默认参数重新计算？",MB_OKCANCEL))
			OnRefresh();
		else 
			return ;
	}
	if((!isFirstRun) && (isSettingChanged) && (!padjust.CheckBoxState))
	{
		if(IDOK==AfxMessageBox("用自定义参数设置计算？",MB_OKCANCEL))
			OnRefresh();
		else 
			return ;
	}
	isFirstRun = FALSE;
	cvFlip(workImg);
	pImg = cvCreateImage(cvGetSize(workImg),IPL_DEPTH_8U,1);

	int thre,ignore;
	if(isSettingChanged)
	{
		thre = padjust.iThreshold;
	}
	else
	{
		thre = OnBasicglobalthreshold();
		padjust.iThreshold = thre;
	}
	ignore = padjust.iIgnore;
	cvCvtColor(workImg,pImg,CV_BGR2GRAY);			   //灰阶化
	cvThreshold(pImg,pImg,thre,255,CV_THRESH_BINARY);  //  二值化
	pContourImg = cvCreateImage(cvGetSize(pImg),IPL_DEPTH_8U,3);
	cvZero(pContourImg);

	mode = CV_RETR_LIST;
//---------------------------------------------------------
	contours_num=cvFindContours(pImg,storage,&contour,  //  轮廓跟踪
		sizeof(CvContour),mode,CV_CHAIN_APPROX_NONE);
//---------------------------------------------------------

	color=2;
	int n;
	double area[256];
	char buf[256];
	cvInitFont(&font,CV_FONT_HERSHEY_COMPLEX,0.4,0.4,0,1,CV_AA);//初始化字体
	for (n=0; contour != 0; contour = contour->h_next,n++) 
	{
		if(n>=255)
		{
			AfxMessageBox("连通区太多，请尝试调整处理参数");
			return ;
		}
		area[n] = cvContourArea(contour);
		if(area[n]<ignore)
		{
			n--;
			continue;
		}
		//找到最小的矩形
		CvBox2D rect = cvMinAreaRect2(contour,storage);
		CvPoint2D32f rect_pts0[4];
		cvBoxPoints(rect, rect_pts0);
		//因为cvPolyLine要求点集的输入类型是CvPoint**
		//所以要把 CvPoint2D32f 型的 rect_pts0 转换为 CvPoint 型的 rect_pts
		//并赋予一个对应的指针 *pt
		int npts = 4;
		CvPoint rect_pts[4], *pt = rect_pts;
		for (int rp=0; rp<4; rp++)
			rect_pts[rp]= cvPointFrom32f(rect_pts0[rp]);
		//画出最小的矩形Box
		cvPolyLine(pContourImg, &pt, &npts, 1, 1, CV_RGB(135,120,20), 1);
		_itoa((int)floor(area[n]),buf,10);
		cvPutText(pContourImg,buf,*pt,&font,CV_RGB(255,255,255));
		external_color = CV_RGB(rand()&255,rand()&255,rand()&255);
		hole_color = CV_RGB(rand()&255,rand()&255,rand()&255);
//		external_color = CV_RGB(VgaDefPal[color].rgbRed,
//			VgaDefPal[color].rgbGreen,VgaDefPal[color].rgbBlue);
//		color=NextColor(2,color,4);         //  1 or 4
		hole_color = CV_RGB(192,192,192);
		cvDrawContours(pContourImg,contour,
				external_color,hole_color,1,1,8);
	}
	AreaDlg dlg;
	CString AreaDetail;
	int iDArea = 0;
	dlg.iTotalArea = pImg->width * pImg->height;
	for(int i=0;i<n;i++)
	{
		int temp = iDArea;
		temp += (int)floor(area[i]);
		if(temp>dlg.iTotalArea)
			break;
		iDArea += (int)floor(area[i]);
		AreaDetail.Format("破损 %d, 面积 %d \r\n",i+1,(int)floor(area[i]));
		dlg.AreaDetail += AreaDetail;
	}
	dlg.iDamageArea = iDArea;
	dlg.iRemainArea = dlg.iTotalArea - iDArea;
	cvReleaseMemStorage(&storage);

	m_dibFlag=imageReplace(pContourImg,&workImg);  //  输出处理结果

	/*if ((n>2)&&(workImg->width>200)&&(workImg->height>150)) {
		sprintf(ch,"%d",contours_num);
		cvPutText(workImg,"contours_num",cvPoint(30,40),&font,CV_RGB(255,255,255));
		cvPutText(workImg,ch,cvPoint(30,60),&font,CV_RGB(255,255,255));
	}*/
	cvFlip(workImg);
	m_ImageType=-2;
 	Invalidate();
	dlg.DoModal();
}

void CMycvProjectView::OnParadjust()
{
	// TODO: 在此添加命令处理程序代码
	if(padjust.DoModal()==IDOK)
	{
		if(padjust.iThreshold != DEFAULT_THRESHOLD || padjust.iIgnore!= DEFAULT_IGNORE)
			isSettingChanged = TRUE;
		int state = padjust.CheckBoxState;
		if(state==0)
			return;
		if(state==1)
		{
			OnRefresh();
			OnAreacalc();
		}
	}
}

void CMycvProjectView::OnFileSaveAs() 
{
    CString csBMP="BMP Files(*.BMP)|*.BMP|";
	CString csJPG="JPEG Files(*.JPG)|*.JPG|";
	CString csTIF="TIF Files(*.TIF)|*.TIF|";
	CString csPNG="PNG Files(*.PNG)|*.PNG|";
	CString csDIB="DIB Files(*.DIB)|*.DIB|";
	CString csPBM="PBM Files(*.PBM)|*.PBM|";
	CString csPGM="PGM Files(*.PGM)|*.PGM|";
	CString csPPM="PPM Files(*.PPM)|*.PPM|";
	CString csSR ="SR  Files(*.SR) |*.SR|";
	CString csRAS="RAS Files(*.RAS)|*.RAS||";

    CString csFilter=csBMP+csJPG+csTIF+csPNG+csDIB
					 +csPBM+csPGM+csPPM+csSR+csRAS;

	CString name[]={"","bmp","jpg","tif","png","dib",
		               "pbm","pgm","ppm","sr", "ras",""};

	CString strFileName;
	CString strExtension;
 
	CFileDialog FileDlg(false,NULL,NULL,OFN_HIDEREADONLY,csFilter);
	                                        //  文件存盘对话框
	if (FileDlg.DoModal()==IDOK ) {         //  选择了文件名
 		strFileName = FileDlg.m_ofn.lpstrFile;
		if (FileDlg.m_ofn.nFileExtension) {  //  无文件后缀
			strExtension = name[FileDlg.m_ofn.nFilterIndex];
			strFileName = strFileName + '.' + strExtension;
			                                //  加文件后缀
		}

		CMycvProjectDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);

		pDoc->Save(strFileName,workImg);   //  当前画面存盘
	}
}

void CMycvProjectView::OnUpdateRefresh(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(!(!workImg));
}

void CMycvProjectView::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
    pCmdUI->Enable(!(!workImg));
}

void CMycvProjectView::OnUpdateAreacalc(CCmdUI* pCmdUI) 
{
    pCmdUI->Enable(!(!workImg));
}

void CMycvProjectView::OnUpdateParadjust(CCmdUI* pCmdUI) 
{
    pCmdUI->Enable(!(!workImg));
}