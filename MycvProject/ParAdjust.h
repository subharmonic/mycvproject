#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// ParAdjust 对话框

class ParAdjust : public CDialog
{
	DECLARE_DYNAMIC(ParAdjust)

public:
	ParAdjust(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~ParAdjust();

	int iThreshold;
	int iIgnore;
	int CheckBoxState;
	virtual BOOL OnInitDialog();
// 对话框数据
	enum { IDD = IDD_PARADJUST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CSliderCtrl m_Slider;
	afx_msg void OnEnChangeEditadjust();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};
