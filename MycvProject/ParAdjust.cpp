// ParAdjust.cpp : 实现文件
//

#include "stdafx.h"
#include "MycvProject.h"
#include "ParAdjust.h"
#include "afxdialogex.h"


// ParAdjust 对话框

IMPLEMENT_DYNAMIC(ParAdjust, CDialog)

ParAdjust::ParAdjust(CWnd* pParent /*=NULL*/)
	: CDialog(ParAdjust::IDD, pParent)
{

}

ParAdjust::~ParAdjust()
{
}

void ParAdjust::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDERIGNORE, m_Slider);
	DDX_Text(pDX,IDC_EDITADJUST,iIgnore);
	DDX_Text(pDX,IDC_EDITTHRE,iThreshold);
	DDX_Check(pDX, IDC_CHECK1, CheckBoxState);
}


BEGIN_MESSAGE_MAP(ParAdjust, CDialog)
	ON_EN_CHANGE(IDC_EDITADJUST, &ParAdjust::OnEnChangeEditadjust)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// ParAdjust 消息处理程序



void ParAdjust::OnEnChangeEditadjust()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

//#1015

	// TODO:  在此添加控件通知处理程序代码
}

BOOL ParAdjust::OnInitDialog()
{
	CDialog::OnInitDialog();
	UpdateData(FALSE);

	m_Slider.SetRange(0,255,TRUE);
	m_Slider.SetPos(iThreshold);

	return TRUE;
}

void ParAdjust::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
	nPos = m_Slider.GetPos();

	CString strText = _T("");
	strText.Format(_T("%d"), nPos);
	SetDlgItemText(IDC_EDITTHRE, strText);

	CString strText2 = _T("");
	GetDlgItemText(IDC_EDITTHRE,strText2);
	m_Slider.SetPos(atoi(strText2));
}
