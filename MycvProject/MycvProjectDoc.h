
// MycvProjectDoc.h : CMycvProjectDoc 类的接口
//

#pragma once

class CMycvProjectDoc : public CDocument
{
protected: // 仅从序列化创建
	CMycvProjectDoc();
	DECLARE_DYNCREATE(CMycvProjectDoc)
	BOOL Load(IplImage** pImg,LPCTSTR pszFilename);
	BOOL Save(LPCTSTR pszFilename,IplImage* pImg);
// 特性
public:
	IplImage*	pImg;
	int			m_Display;
// 操作
public:

// 重写
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// 实现
public:
	virtual ~CMycvProjectDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// 用于为搜索处理程序设置搜索内容的 Helper 函数
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
