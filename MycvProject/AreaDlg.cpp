// AreaDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MycvProject.h"
#include "AreaDlg.h"
#include "afxdialogex.h"


// AreaDlg 对话框

IMPLEMENT_DYNAMIC(AreaDlg, CDialog)

AreaDlg::AreaDlg(CWnd* pParent /*=NULL*/)
	: CDialog(AreaDlg::IDD, pParent)
{
	iTotalArea = 0;
	iDamageArea = 0;
	iRemainArea = 0;
	AreaDetail = _T("");
}

AreaDlg::~AreaDlg()
{
}

void AreaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TOTALAREA, iTotalArea);
	DDX_Text(pDX,IDC_DAMEGEAREA,iDamageArea);
	DDX_Text(pDX, IDC_REMAINAREA, iRemainArea);
	DDX_Text(pDX,IDC_AREADETAIL,AreaDetail);
}


BEGIN_MESSAGE_MAP(AreaDlg, CDialog)
END_MESSAGE_MAP()


// AreaDlg 消息处理程序

