
// MycvProjectDoc.cpp : CMycvProjectDoc 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MycvProject.h"
#endif

#include "MycvProjectDoc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMycvProjectDoc

IMPLEMENT_DYNCREATE(CMycvProjectDoc, CDocument)

BEGIN_MESSAGE_MAP(CMycvProjectDoc, CDocument)
END_MESSAGE_MAP()


// CMycvProjectDoc 构造/析构

CMycvProjectDoc::CMycvProjectDoc()
{
	// TODO: 在此添加一次性构造代码
	pImg=NULL;
 	m_Display=-1;
}

CMycvProjectDoc::~CMycvProjectDoc()
{
	if (pImg)
		cvReleaseImage(&pImg);
}

BOOL CMycvProjectDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 在此添加重新初始化代码
	// (SDI 文档将重用该文档)
	return TRUE;
}




// CMycvProjectDoc 序列化

void CMycvProjectDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}

#ifdef SHARED_HANDLERS

// 缩略图的支持
void CMycvProjectDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// 修改此代码以绘制文档数据
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 搜索处理程序的支持
void CMycvProjectDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// 从文档数据设置搜索内容。
	// 内容部分应由“;”分隔

	// 例如:  strSearchContent = _T("point;rectangle;circle;ole object;")；
	SetSearchContent(strSearchContent);
}

void CMycvProjectDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CMycvProjectDoc 诊断

#ifdef _DEBUG
void CMycvProjectDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMycvProjectDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMycvProjectDoc 命令
BOOL CMycvProjectDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here
	BOOL boolean;

    Load(&pImg,lpszPathName);
	if (pImg!=NULL) 
		boolean=true;
	else 
		boolean=false;
	return(boolean);
}

//---------------------------------------------------------
//---------------------------------------------------------
//  ImageIO
 
BOOL CMycvProjectDoc::Load(IplImage** pp,LPCTSTR csFileName)
{
	IplImage* pImg=NULL;


	pImg = cvLoadImage(csFileName,-1);      //  读图像文件(DSCV)
	if (!pImg){
		AfxMessageBox("打开图像失败，请检查图像格式和路径是否正确");
		return(false);
	}
 	cvFlip(pImg);                           //  与 DIB 像素结构一致
 	if (*pp) {
		cvReleaseImage(pp);
	}
	(*pp)=pImg;
   	m_Display=0;
	return(true);
}

BOOL CMycvProjectDoc::Save(LPCTSTR csFileName,IplImage* pImg)
{
	int   bl;

 	cvFlip(pImg);                           //  恢复原 OpenCV 位图结构
  	bl=cvSaveImage(csFileName,pImg);        //  图像存盘
 	return(bl);
}