
// MycvProjectView.h : CMycvProjectView 类的接口
//
#include "ParAdjust.h"
#pragma once

class CMycvProjectView : public CScrollView
{
protected: // 仅从序列化创建
	CMycvProjectView();
	DECLARE_DYNCREATE(CMycvProjectView)

// 特性
public:
	CMycvProjectDoc* GetDocument() const;
	ParAdjust padjust;

// 操作
public:
	int CMycvProjectView::OnBasicglobalthreshold();

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate(); // called first time after construct
protected:

// 实现
public:
	virtual ~CMycvProjectView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	IplImage* saveImg;
	IplImage* workImg;
 
	LPBITMAPINFO m_lpBmi;

	int     m_dibFlag;
	int     m_SaveFlag;
 	int     m_ImageType;

	bool	isFirstRun;
	bool	isSettingChanged;
// 生成的消息映射函数
protected:
	//afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnRefresh();
	afx_msg void OnAreacalc();
	afx_msg void OnParadjust();
	afx_msg void OnFileSaveAs();

	afx_msg void OnUpdateRefresh(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAreacalc(CCmdUI* pCmdUI);
	afx_msg void OnUpdateParadjust(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
};

#ifndef _DEBUG  // MycvProjectView.cpp 中的调试版本
inline CMycvProjectDoc* CMycvProjectView::GetDocument() const
   { return reinterpret_cast<CMycvProjectDoc*>(m_pDocument); }
#endif

