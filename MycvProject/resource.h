//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 MycvProject.rc 使用
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_MycvProjectTYPE             130
#define IDD_AREADLG                     310
#define IDD_PARADJUST                   311
#define IDC_TOTALAREA                   1000
#define IDC_DAMEGEAREA                  1001
#define IDC_DAMAGEAREA                  1001
#define IDC_REMAINAREA                  1002
#define IDC_AREADETAIL                  1003
#define IDC_SLIDERIGNORE                1004
#define IDC_EDITADJUST                  1005
#define IDC_EDIT2                       1006
#define IDC_EDITTHRE                    1006
#define IDC_CHECK1                      1007
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_OnRefresh                    32773
#define ID_Refresh                      32774
#define ID_BASICGLOBALTHRESHOLD         32775
#define ID_32776                        32776
#define ID_COLORTOGRAY                  32777
#define ID_32778                        32778
#define ID_TDOTSU                       32779
#define ID_32780                        32780
#define ID_AREACALC                     32781
#define ID_FILE_NEW_                    32782
#define ID_FILE_SAVE_AS_                32783
#define ID_EDIT_FIND_                   32784
#define ID_EDIT_COPY_                   32785
#define ID_EDIT_PASTE_LINK_             32786
#define ID_FILE_PRINT_                  32787
#define ID_32788                        32788
#define ID_PARADJUST                    32789
#define ID_32790                        32790

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        311
#define _APS_NEXT_COMMAND_VALUE         32791
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           312
#endif
#endif
