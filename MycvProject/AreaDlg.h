#pragma once


// AreaDlg 对话框

class AreaDlg : public CDialog
{
	DECLARE_DYNAMIC(AreaDlg)

public:
	AreaDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AreaDlg();

// 对话框数据
	enum { IDD = IDD_AREADLG };
	int iTotalArea;
	int iDamageArea;
	int iRemainArea;
	CString AreaDetail;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit1();
};
